package logica;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Contador {

	
	private int cuenta;
	
	
	public Contador () {
		this.cuenta = 0;
	}

	    public void incrementar()
	    {
	    	cuenta ++;
	    	
	    }
	    
	    public int getCuenta() {
			return cuenta;
		}



	    public void setCuenta(int cuenta) {
			this.cuenta = cuenta;
		}



	    public  void guardarConteo() {
		

	        try {
	            //Crear un objeto File se encarga de crear o abrir acceso a un archivo que se especifica en su constructor
	            File archivo = new File("Datos/texto.txt");

	            //Crear objeto FileWriter que sera el que nos ayude a escribir sobre archivo
	            FileWriter escribir = new FileWriter(archivo, true);


	            //Escribimos en el archivo con el metodo write 
	  
	                escribir.write("Nueva cuenta:");
	                escribir.write("\r\n");
	                escribir.write("Total: " + this.cuenta ); 
	                escribir.write("\r\n");

	            

	            //Cerramos la conexion
	            escribir.close();
	            
	            resetCuenta();
	            
	        } //Si existe un problema al escribir cae aqui
	        catch (Exception e) {
	            System.out.println("Error al escribir");
	        }
	        
		}

		private void resetCuenta() {
			this.setCuenta(0);
		}
	}